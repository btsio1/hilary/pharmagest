

package sql;

import classes.Fournisseur;
import classes.Livraison;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sample.BD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class impfournisseur implements Ifournisseur {
    private static Connection conn = BD.connect();

    @Override
    public boolean ajouterfournisseur(String numtlf, String name) throws SQLException {
        try {
            PreparedStatement ps = conn.prepareStatement("INSERT INTO fournisseur (numtel, nom) VALUES (?, ?)");
            ps.setString(1, numtlf);
            ps.setString(2, name);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean update(int id, String numtlf, String name) {
        try {
            PreparedStatement ps = conn.prepareStatement("UPDATE fournisseur SET numtel = ?, nom = ? WHERE idfournisseur = ?");
            ps.setString(1, numtlf);
            ps.setString(2, name);
            ps.setInt(3, id);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(int id) {
        try {
            PreparedStatement ps = conn.prepareStatement("DELETE FROM fournisseur WHERE idfournisseur = ?");
            ps.setInt(1, id);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public ObservableList<Livraison> listelivraison(int id) throws SQLException {
        ObservableList<Livraison> list= FXCollections.observableArrayList();
        PreparedStatement ps=conn.prepareStatement("SELECT idlivraison FROM livraison WHERE idfournisseur = ?");
        ps.setInt(1, id);
        ResultSet rs=ps.executeQuery();
        while(rs.next()){
            list.add(new Livraison(rs.getInt("idlivraison")));
        }
        return list;
    }

    @Override
    public ObservableList<Fournisseur> listefournisseur() throws SQLException {
        ObservableList<Fournisseur> list= FXCollections.observableArrayList();
        PreparedStatement ps=conn.prepareStatement("SELECT * FROM fournisseur ");
        ResultSet rs=ps.executeQuery();
        while(rs.next()){
            list.add(new Fournisseur(rs.getInt("idfournisseur"),rs.getString("numtel"),rs.getString("nom")));
        }
        return list;
    }

    @Override
    public int getid(String num) throws SQLException {
        PreparedStatement ps=conn.prepareStatement("SELECT idfournisseur FROM fournisseur WHERE numtel = ?");
        ps.setString(1, num);
        ResultSet rs=ps.executeQuery();
        if(rs.next()) {
            return rs.getInt("idfournisseur");
        } else {
            return 0;
        }
    }
}

