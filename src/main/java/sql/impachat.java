
package sql;

import sample.BD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.time.LocalDate;

public class impachat implements iachat {
    private static Connection conn = BD.connect();

    @Override
    public boolean ajoutermedprescrit(int idachat, int idmedprescrit, int qte, LocalDate dure, String nom) {
        try {
            PreparedStatement ps = conn.prepareStatement("insert into medicamentprescrit (idmedprescrit, idachat, qte, dure, nom) values (?, ?, ?, ?, ?)");
            ps.setInt(1, idmedprescrit);
            ps.setInt(2, idachat);
            ps.setInt(3, qte);
            ps.setObject(4, dure);
            ps.setString(5, nom);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
